<?php

namespace Pillbox\Magento\Command\Order;

use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CloseOrder extends AbstractMagentoCommand
{

    const ENTITY                            = 'order';
    const EMAIL_EVENT_NAME_NEW_ORDER        = 'new_order';
    const XML_PATH_EMAIL_TEMPLATE           = 'sales_email/order/template';
    const XML_PATH_EMAIL_IDENTITY           = 'sales_email/order/identity';


    protected function configure()
    {
        $this
            ->setName('order:close')
            ->setDescription('Closes the specified order. [Pillbox]')
            ->addArgument('increment_id', InputArgument::REQUIRED, 'order number to be closed')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->detectMagento($output, true);
        $increment_id = $input->getArgument('increment_id');
        if ($this->initMagento() && !is_null($increment_id)) {

          $write = \Mage::getSingleton('core/resource')->getConnection('core_write');
          $sqls = array(
            "UPDATE sales_flat_order SET status='closed', state='closed' WHERE increment_id=$increment_id",
            "UPDATE sales_flat_order_grid SET status='closed' WHERE increment_id=$increment_id",
          );

          foreach($sqls as $sql)
          {
            $write->exec($sql);
          }

          echo "Order " . $increment_id . " has been marked as closed.\n";

        }
    }
}
